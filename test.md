---
documentclass: lettre-UTC
---

![Chaine de transmission des coquilles chez le Bernard l'Hermite](image.png){width="327"}

# Agent Bernard oyé yihha ici

## Objectif

L'agent *Bernard* cherche à survivre le plus longtemps possible, pour se faire il est toujours à la recherche d'une meilleure coquille, toujours à sa taille, et de bonne qualité.

## Attributs

-   L'agent *Bernard* possède une taille qui augmente de façon linéaire en fonction du temps.
-   L'agent *Bernard* possède ou non une coquille (règle d'or: la taille de la coquille doit être supérieure à celle de *Bernard*)
-   L'agent *Bernard* possède une jauge de nourriture.
-   L'agent *Bernard* possède une vitesse de déplacement réelle (vitesse initiale ralentie par la différence entre la taille de la coquille et celle de Bernard)

## Constantes

-   Distance de perception
-   Taille de la jauge de nourriture
-   Vitesse de déplacement initiale (de base)
-   Temps de survie sans coquille (peut-être supérieur pour les bébé *Bernards*)

## Règles d'apparition

-   Les bébés *Bernards* sortent progressivement de la mer sans coquille avec une taille 1.

## Règles de survie

-   L'agent *Bernard* sans coquille entre en mode survie, il meurt au bout de N itérations tué par un prédateur.
-   L'agent *Bernard* meurt si sa jauge de nourriture est nulle.

## Algorithme de recherche

-   Recherche de nourriture
-   Recherche de coquille
-   En mode survie, l'agent *Bernard* cherche uniquement une coquille vide à sa taille.

## Algorithme de choix de coquille

-   À définir

\newpage

# Agent Coquille

## Attributs

-   L'agent *Coquille* possède une taille fixe définie à sa création

-   L'agent *Coquille* appartient ou non un agent *Bernard*

-   L'agent *Coquille* possède une usure qui augmente de façon linéaire en fonction du temps

## Constantes

-   Taille maximale d'une coquille

## Règles de déplacement

-   L'agent *Coquille* immergé sous l'eau se déplace aléatoirement d'une unité de surface.

# Agent Nutriment

## Attributs

-   L'agent *Nutriment* possède un nombre de consommation. Si cet attribut est égal à 0, l'agent disparaît.

## Constantes

-   Nombre maximum de consommation

## Règles de déplacement

-   L'agent *Coquille* immergé sous l'eau se déplace aléatoirement d'une unité de surface.
-   Salut

# Environnement Marée chaude salante

La marée apparait toutes les N itérations, elle réduit en sable les coquilles usées et amène de nouvelles coquilles et de nouveaux nutriments aléatoirement.

| Yo  | Hey | Salut |
|-----|-----|-------|
|     |     |       |
|     |     |       |
|     |     |       |

lsdkfjsdFELIXlmkfjdslfjdslfjd
